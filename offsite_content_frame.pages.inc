<?php

/**
 * @file
 * Menu page callbacks for the offsite content frame module.  Contains
 * a single page callback, offsite_content_frame_page, which overrides
 * the default drupal page layout system and replaces it with a single
 * toolbar and iframe.
 */

function offsite_content_frame_page($remote_url, $referring_url) {

  ctools_set_no_blocks();
  offsite_content_frame_is_frame_page(TRUE);

  return theme('offsite_content_frame_iframe', array(
    'remote_url' => urldecode($remote_url),
    'referring_url' => urldecode($referring_url),
    'site_name' => variable_get('site_name'),
  ));
}