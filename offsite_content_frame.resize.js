(function ($) {

  Drupal.behaviors.offsiteContentFrameResize = {
    attach: function (context, settings) {

      var iframe = $(context).find("#offsite-content-frame-iframe"),
        header = $(context).find("#offsite-content-header-wrapper"),
        header_height = header.outerHeight(true),
        $window = $(window);

      if (iframe.length === 1) {

        iframe.height($window.height() - header_height);

        $window.resize(function () {

          iframe.height($window.height() - header_height);
        });
      }
    }
  };
}(jQuery));