<?php

/**
 * @file
 * The offsite_content_frame module's theme implementation to display a
 * single Drupal page.  This page removes all blocks and most page elements
 * and instead displays a simple header bar at the top and an iframe
 * with the offsite content embedded in it, fed from 
 * offsite_content_frame_iframe.tpl.php.
 *
 * All values / variables provided to the default page.tpl.php implementation
 * are available here, though using them would kinda defeat the purpose of
 * this template.
 */
?>
<div id="offsite-content-frame-page">
  <?php print render($page['content']); ?>
</div>
