<?php

/**
 * @file
 * The offsite_content_frame module's theme implementation to display an
 * iframe containing a remote site, and a toolbar allowing users to dismiss
 * the toolbar or return back to the page they were viewing.
 *
 * The following values are provided to the template:
 *   - $site_name (string)
 *       The set site name of the current Drupal install
 *   - $remote_url (string)
 *       The URL to be rendered in the iframe
 *   - $referring_url (string)
 *       The URL that the vistor was on when then clicked the link to bring
 *       to the off site content.
 *
 */
?>
<div id="offsite-content-header-wrapper">
  <div id="offsite-content-header">
    <span id="offsite-content-return-link" class="offsite-content-exclude">
      <?php echo l(t('Remove frame'), $remote_url); ?>
    </span>
    <h1 id="page-title">
      <?php echo l(t('← Back to !site-name', array('!site-name' => $site_name)), $referring_url); ?>
    </h1>
  </div>
</div>
<iframe id="offsite-content-frame-iframe" src="<?php echo $remote_url; ?>"></iframe>