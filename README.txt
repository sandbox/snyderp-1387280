Offsite Content Frame
===

About
---

offsite_content_frame captures all external links on your site and redirects
them to a page, on your site, with the external content in an iframe.  A link
back to your site is included in the header, along with an option to dismiss
the frame.


Usage
---

Enable the offsite_content_frame module the way you would any other module,
through the admin modules page.  Everything else is done automatically.

 - snyderp <snyderp@gmail.com>