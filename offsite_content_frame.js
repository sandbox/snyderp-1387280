(function ($) {

  Drupal.behaviors.offsiteContentFrame = {
    attach: function (context, settings) {

      // For every anchor in the page that isn't specifically marked
      // to be excluded from our offsite content link grabbing,
      // check to see if the link is remote, and if it is, point
      // it to the offsite-content frame
      $(context)
        .find("a")
          .not("a.offsite-content-exclude, .offsite-content-exclude a")
          .each(function (index, element) {
    
        var element_href = element.href.toLowerCase(),
          current_domain = settings.offsiteContentFrame.domain,
          remote_url,
          local_url = encodeURIComponent(encodeURIComponent(window.location.href));
    
        if (element_href.indexOf('http://') === 0 || element_href.indexOf('https://') === 0) {
    
          if (element_href.indexOf(current_domain) === -1) {

            remote_url = encodeURIComponent(encodeURIComponent(element_href));

            $(element).attr("href", "/offsite-content/" + remote_url + "/" + local_url);
          }
        }
      });
    }
  };
}(jQuery));